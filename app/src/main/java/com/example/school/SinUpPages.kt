package com.example.school

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatButton

class SinUpPages : AppCompatActivity() {
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sin_up_pages)
        val sinupBtn = findViewById<AppCompatButton>(R.id.sinup_btn)
        sinupBtn.setOnClickListener {
            var confirmAlert = AlertDialog.Builder(this)
            confirmAlert.setTitle("Confirmation")
            confirmAlert.setMessage("Are You Sure")
            confirmAlert.setCancelable(false)
            confirmAlert.setPositiveButton("Yes"){_,_ ->
                Toast.makeText(this, "Yes", Toast.LENGTH_SHORT).show()
            }
            confirmAlert.setNegativeButton("No"){_,_ ->
                Toast.makeText(this, "No", Toast.LENGTH_SHORT).show()
            }
            confirmAlert.create().show()
        }
    }
}